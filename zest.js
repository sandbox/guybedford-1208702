//>>>>>> BEGIN ZEST CORE >>>>>>

//it would be awesome if hooks collapsed into one function on the element
//that all the existing hooks are taken together and 'compiled' into the hook function on the element
//so you don't need to write el._('load'), you just do el._load.
//then to remove an element you use el._destroy, and all the destroying processes happen

//how to just create something once
//eg popups - $z.register('popup', ... ) //creates html $z.create('popup')
//well you may want many popups. to just want one, is to destroy it when it closes itself

//can plugins not be a form of inheritance?
//let elements specify those plugins they inherit from

//dependencies

//generalised enhancements (things like anything with class red gets turned red etc... css is almost this, or things like clearclick on an input)
//now if an input expects this, it adds the class, then awaits the enhancement
//but we could skip this class stuff and define it in the widget through an 'add enhancement' interface
//and then have a number of enhancements that can act
//so we have widgets and enhancements as separate things?
//could love to bring in css management as well

//how to capture these into widgets as well?

//along the lines of:
//this.addEnhancement('clickclear');

//or in definition _dependencies: 'enhancements.clickclear, drupal.autocomplete' etc

//i'm also thinking the hooks can be removed since the plugin format has dissolved
//rather, plugins are either just things that widgets can extend or link into
//say:

//$z.create({
//  _plugins: $z.enhancements, $z.tpl etc
//});

//i'd like to simplify the architecture, not complicate more though
//think of simple things, like i want to be able to have a function called hide, which can be added through a plugin
//NB this needs to thought about with respect to integration with other frameworks
//the easier it is for other frameworks to link with zest the better

//think carefully about letting go of hooking... the alternative is direct adding to the core object... but the point of hooking is to provide an interface to this
//perhaps you just need to clarify the way the hooking interface works in these general cases?


//HOW ABOUT CREATING AN ELEMENT THAT ISN't REGISTERED, by defining it anonymously?
//Id like to be able to easily wrap an existing widget, and anonymously add functionality
//which is great for create, but on enhance selectors, this isn't easy....
  
  
//nb hooks are useful to generalise interfaces
//eg state changes etc.
//also to generalise inheritance and addon interfaces
//or even data interfaces on an element
//so maybe what we really mean by 'hook' is the ability to
//have a function or method on an element of a certain form
//mywidget _interfaces ['states', '', ..]
//then we immediately know methods on the element.
//we can also build those methods in a public representation
//pubElement.stateChange = function(){return $z.elements[this].stateChange.apply(element, arguments)};
//i like this direction
//perhaps the current form isn't quite right yet

//could enforce templating (when not enhancing)
//it's always needed
//while it's good to provide flexibility, it's also good to enforce convention
//if someone wants to break a convention, there's normally a loophole, but they'll know they're using a loophole

//****** Zest Function and Namespace Setup ******
$z = function(widgetSelector){
  function enhanceCheck(elem){
    if($z.enhances[i].check(elem))
      $z.enhance($z.enhances[i].type, elem);
  }
  //run any selector enhancements
  for(var i = 0; i < $z.enhances.length; i++){
    var elems = $z.$($z.enhances[i].selector);
    for(var j = 0; j < elems.length; j++)
      enhanceCheck(elems[j]);
  }
  
  //instance checking
  var out = [];
  for(var i = 0; i < $z.instances.length; i++){
    //check if instances are in the main dom, and if so, run their load event
    if(!$z.instances[i].loaded){
      //check if element is in the dom
      if($z._utils.inDOM($z.instances[i].$z.$$[0]))
	$z.instances[i].$z._('load');
    }
    
    //if a widget selector is provided, then find the instance
    if(widgetSelector){
      if($z.instances[i].type == widgetSelector)
	out.push($z.instances[i].$z);
    }
  }
  
  //widget selector returning
  if(out.length == 1) out = out[0];
  if(out.length == 0) out = undefined;
  return out;
};

//zest global settings
$z.settings = {
  live: false //live dom monitoring (automatically runs $z() to refresh load and enhance events)
}; 
$z.widgets = {}; //widgets
$z.fn = {}; //plugins
$z.tpl = {}; //templates
$z.instances = []; //global instance store
$z.enhances = []; //enhance selector for live attachment

//return the instance meta-data for a given zest object
$z.getInstance = function(obj){
  for(var i = 0; i < $z.instances.length; i++)
    if($z.instances[i].$z === obj) return $z.instances[i];
  return null;
}


//****** Utitiliy Functions ******
$z._utils = {
  //namespacing
  //returns the name of the namespace item (eg heading from my.widgets.heading)
  name: function(ns){
    return ns.split('.').pop();
  },
  //returns if the element is in the page or not
  inDOM: function(e){
    if(e == document) return true;
    if(!e.parentNode) return false;
    return $z._utils.inDOM(e.parentNode);
  },
  //builds / gets an object from that part of the namespace (eg $z.widgets.my.widgets.heading)
  widget: function(ns, obj){
    var names = ns.split('.');
    var curObj = $z.widgets;
    for(var i = 0; i < names.length - 1; i++)
      curObj = curObj[names[i]] = (curObj[names[i]] != undefined ? curObj[names[i]] : {});
    
    //get
    if(obj == null){
      if(!curObj[$z._utils.name(ns)]) throw('Zest error: Widget "' + ns + '" is undefined.');
      return curObj[$z._utils.name(ns)];
    }
    //set
    else{
      if(curObj[$z._utils.name(ns)] != undefined) throw('Zest error: Widget "' + ns + '" is already defined.');
      return curObj[$z._utils.name(ns)] = obj;
    }
  },
  //main hooking
  hook: function(hook, options, obj, ns){
    //command function runs widget command hooks, which can be bound to functions
    var self = (obj != null) ? obj : this;
    
    //if this has a _type, load the widget to include for hooking
    var widget = null;
    if(ns != undefined) widget = $z._utils.widget(ns);
    
    //prepare arguments
    var args = [options, widget];
    
    //call plugin, widget and option hooks
    //by default return the zest object, or last valid hook return
    var result = self, curResult; 
    for(var f in $z.fn){
      if(typeof $z.fn[f]['_' + hook] == 'function'){
	curResult = $z.fn[f]['_' + hook].apply(self, args);
	//only track return if a return value is given
	if(!(curResult === undefined)) result = curResult;
      }
    }
    if(typeof widget['_' + hook] == 'function')
      curResult = widget['_' + hook].apply(self, args);
    if(typeof options['_' + hook] == 'function')
      curResult = options['_' + hook].apply(self, args);
    
    if(!(curResult === undefined)) result = curResult;
    return result;
  },
  //extending without copying '_' properties
  extend: function(target, object, exclude_){
    var makeFunc = function(func, self){
      return function(){ return func.apply(self, arguments); };
    };
    
    for(var o in object){
      if(o.substr(0, 1) == '_' && exclude_) continue;
      
      if(typeof object[o] == 'function')
	target[o] = makeFunc(object[o], target);
      else
	target[o] = object[o];
    }
    return target;
  }
};

//****** Widget Registration and Creation ******

$z.register = function(ns, widget){
  //add widget to namespace under $z.widgets.asdf.asdf
  $z._utils.widget(ns, widget);
  
  //add selector auto enhancement
  if(widget._selector){
    $z.enhances.push({
      type: ns,
      selector: widget._selector,
      check: widget._check ? widget._check : function(){ return true; }
    });
    //run zest for live attachment
    $z();
    return;
  }
};
$z.destroy = function(widget){
  //instance checking
  var out;
  var newinstances = [];
  for(var i = 0; i < $z.instances.length; i++){
    if($z.instances[i].type === widget || $z.instances[i].$z === widget){
      out = $z.instances[i].$z._('destroy');
    }
    else
      newinstances.push($z.instances[i]);      
  }
  $z.instances = newinstances;
  return out;
};

$z.create = function(options, obj){
  
  //------ Parameter Checking ------//

  if(obj == null) obj = {};

  //if options is an array of options, then create multiple widgets
  if(obj.length > 0){
    var output = [];
    for(var i = 0; i < obj.length; i++)
      output.push($z.create(options, obj[i]));
    return output;
  }

  if(typeof options == 'string'){
    if(!obj._type) obj._type = options; //let a _type override this string though
    return $z.create(obj);
  }
  
  //check that we have a _type
  if(options._type == undefined) throw ('Zest Error: You need to specify the "_type" parameter of the widget to create an instance.');

  //------ Widget Creation ------//
  
  var widget = $z._utils.widget(options._type);
  
  if(obj == null) obj = {};
  
  //extend the object with the plugins, widget and instance definitions (binding functions and objects by reference, and excluding _hooks)
  for(var f in $z.fn)
    $z._utils.extend(obj, $z.fn[f], true);
  $z._utils.extend(obj, widget, true);
  $z._utils.extend(obj, options, true); //this one may change in due course
  
  //create hook method, building in the element type
  obj._ = function(hook, opts){ return $z._utils.hook.apply(obj, [hook, $z._utils.extend(options, opts), obj, options._type]); };
  
  //add the object to the global instance store, with meta-data
  $z.instances.push({
    type: options._type,
    $z: obj,
    loaded: false
  });
  
  //hook init
  return obj._('init');
};


//****** Element Creation and Enhancement ******

$z.fn.core = {
  _init: function(options, widget){
    if(this.$$) return; //only run to create an element if there isn't one
    
    //run create hook
    var output = this._('create');
    if(this.$$ == undefined) this.$$ = output; //output returns element
    if(this.$$) output = this.$$; //already set $$ in _create
    //if no element is created throw a creation error
    if(this.$$ == null) throw("Zest Error: Element not defined during creation for " + options._type);

    //if its not an array, make it an array
    if(!this.$$.length) this.$$ = [this.$$];

    //finally, add the zest reference
    this.$$[0].$z = this;
    
    //run attach hook
    this._('attach');
    
    //return object
    return output;
  },
  _load: function(){
    //mark the element as loaded
    $z.getInstance(this).loaded = true;
  }
};

$z.enhance = function(ns, e, options){
  if(e.length == 0) return;
  //allow this function to act on an element array
  if(e.length > 0){
    var results = [];
    for(var i = 0; i < e.length; i++)
	results.push($z.enhance(ns, e[i], options));
    return results;
  }
  
  //if it's already enhanced, skip
  if(e.$z) return;
  
  //------ start enhancement ------
  var widget = $z._utils.widget(ns);
  
  //create the element, with the dom element as its base
  var z = $z.create($z._utils.extend({_type: ns}, options), {$$: [e]});
  
  //add the zest reference
  e.$z = z;
  
  //run the progressive enhancement hook on the element
  z._('enhance');
  
  //run attach hook
  z._('attach');
 
  //because enhancement is designed for elements in the dom, run the load function
  z._('load');
 
  return e;
};


//****** Templating ******

$z.tpl.html = function(tpl){ return tpl; } //the html identity template
$z.fn.template = {
  _create: function(options, widget){
    var template = widget._template ? widget._template : this._template;
    if(!template) return; //no template
    
    var lang, html;
    //if a template language is specified, use that
    if(widget._template_language){
      lang = widget._template_language;
      if($z._tpl[lang] == undefined)
	throw('Zest Error: Template language ' + lang + ' is not defined.');
    }
    //otherwise take most recently defined template language
    else for(lang in $z.tpl) ;

    //create template
    var wrapper = document.createElement('DIV');
    wrapper.innerHTML = $z.tpl[lang].call(this, widget._template);
    
    //add the collection references on the managing element
    this.$$ = [];
    for(var i = 0; i < wrapper.childNodes.length; i++)
      this.$$.push(wrapper.childNodes[i]);
    
    //return element
    return $(this.$$);
  }
};

//------ Simple Template ------
//$z.create({$title: 'hello world', $content: 'it\'s a nice day today'});
$z.tpl.simple_template = function(tpl){
  //find all options beginning with a $ sign, and replace with the property on the element
  var html = tpl;
  for(var o in this)
    if(o.substr(0, 1) == '$' && typeof this[o] in {string:1, number:1})
      html = html.replace(new RegExp('\\\$' + o.substr(1), 'g'), this[o]);
  
  //if there are any remaining $ vars, just replace them with blank text
  return html.replace(new RegExp('\\\$[a-zA-Z\-]*', 'g'), '');
};

//function shortcut
//often need this.click(this.onClick), but then lose scope of 'this'
//do this.click(this._f('onClick'));
$z.fn.applyscope = {
  _scope: function(fn){
    var self = this;
    return (function(){
      fn.apply(self, arguments);
    });
  }
};

//****** Framework-Specific Code ******

(function($){
  if(jQuery){

    //------ jQuery plugin ------
    $.fn.$z = function(cmd){
      //if there's no zest object, return
      if(this[0].$z == undefined) return null;
      //if the cmd is null, then return the zest object for the nearest element
      if(cmd == null) return this[0].$z;
      //otherwise, run command on the element
      if(this[0].$z[cmd]) return this[0].$z[cmd];
      return null;
    };
    
    $.fn.$z_enhance = function(type, options){
      //return the enhanced element array - this also runs 'text' as a command if necessary
      return $z.enhance(type, this, options);
    };
  
    //------ jQuery selectors and wrappers -----
    $z.$ = $;
    $z.fn.selectors = {
      //for created elements, can only set the element at attach
      _attach: function(){
	if(this.$$){
	  this.$$ = $(this.$$);
	  this.$$ = $(this.$$);
	}
	
	if(this.$) return;
	var self = this;
	this.$ = function(selector){
	  return $.merge($(selector, self.$$), $(self.$$).filter(selector));
	}
      },
      //for enhanced elements, can do this from the start
      _enhance: function(){
	if(this.$$){
	  this.$$ = $(this.$$);
	  this.$$ = $(this.$$);
	}
	
	if(this.$) return;
	var self = this;
	this.$ = function(selector){
	  return $.merge($(selector, self.$$), $(self.$$).filter(selector));
	}
      }
    };
  
    //------ DOM change ------
    function jQueryBind(f){
      if(!$.fn[f]) return;
      var old = $.fn[f];
      $.fn[f] = function(){
	var r = old.apply(this, arguments);
	if($z.settings.live) $z();
	return r;
      }
    };
    var l = ['after', 'wrap', 'append', 'appendTo', 'before', 'detach', 'empty', 'html', 'prepend', 'prependTo', 'replaceAll', 'replaceWith', 'unwrap', 'wrapAll', 'wrapInner'];
    for(var i = 0; i < l.length; i++)
      jQueryBind(l[i]);
    
    //------ DOM ready ------
    $(document).ready($z);
    
    //------ Visibility Plugin ------
    $z._utils.isVisible = function(e){
      if(!e) return false;
      if(e == document) return true;
      if($(e).css('display') == 'none' || $(e).css('visibility') == 'hidden')
	return false;
      if(!e.parentNode) return false;
      return $z._utils.isVisible(e.parentNode);
    }
  }
  
})(jQuery);

//<<<<<< END OF ZEST CORE <<<<<<
