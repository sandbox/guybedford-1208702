<?php
/**
 * Implementation of views_plugin_style
 *
 */

class views_plugin_style_simple_carousel extends views_plugin_style {
  
  /**
   * Set default options
   */
  function option_definition(){
    $options = parent::option_definition();
    $options['autoscroll'] = array('default' => 'No');
    $options['scrollcount'] = array('default' => '1');
    
    return $options;
  }
  
  /**
   * Provide a form for setting options.
   *
   * @param array $form
   * @param array $form_state
   */
  function options_form(&$form, &$form_state){
    $form['autoscroll'] = array(
      '#type' => 'select',
      '#title' => t('Auto Scroll'),
      '#options' => array('Yes' => t('Yes'), 'No' => t('No')),
      '#default_value' => $this->options['autoscroll'],
    );
    
    $form['scrollcount'] = array(
      '#type' => 'textfield',
      '#title' => t('Scroll Count'),
      '#default_value' => $this->options['scrollcount'],
      '#size' => 5,
    );
  }
}



?>