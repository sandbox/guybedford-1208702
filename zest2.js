//Zest JavaScript widget framework
//Guy Bedford 2011
//
// if there's no object, _object is called [special hook]
// init called on creation, after adding all the methods from plugins and the template
// if not progressively enhanced, create is called
// then in both cases, attach is called to add any events

/* Widget System - main use is to separate logic from html*/
//initiall creates just one hook - init
//then just handles passing around data and namespacing with plugins



$z = function(ns, widget){
  
  //utils singleton
  Utils = (function(){
    return {
      //namespacing
      //returns the name of the namespace item (eg heading from my.widgets.heading)
      name: function(ns){
	Utils.names = ns.split('.');
	return Utils.names[Utils.names.length - 1];
      },
      //builds / gets an object from that part of the namespace (eg $z.my.widgets.heading)
      nameSpace: function(ns, obj){
	Utils.name(ns);
	var curObj = $z;
	for(var i = 0; i < Utils.names.length - 1; i++)
	  curObj = curObj[Utils.names[i]] = curObj[Utils.names[i]] != undefined ? curObj[Utils.names[i]] : {};
	if(obj == null)
	  return curObj[Utils.name(ns)];
	else
	  return curObj[Utils.name(ns)] = obj;
      },
      //extending without copying '_' properties
      extend: function(target, object){
	for(var o in object)
	  if(o.substr(0, 1) != '_')
	    target[o] = object[o];
      }
    }
  })();
  
  //widget instance creation function
  var makeWidget = function(widget, options, ns, obj){
    //if options is an array of options, then create multiple widgets
    if(options.length > 0){
      var output = [];
      for(var i = 0; i < options.length; i++)
	output.push(makeWidget(widget, options[i], ns, obj)[0]);
      return output;
    }
    
    //if widget is empty or we have a special type definition, take widget from options type
    if(widget == null || options._type != undefined){
      var t = options._type;
      options._type = undefined;
      return Utils.nameSpace(t)(options, obj);
    }
    
    var hook = function(hook, obj){
      //command function runs widget command hooks, which can be bound to functions
      var self = (obj != null) ? obj : this;
      //create new arguments array without hook name [basic shift]
      var args = [];
      for(var i = 1; i < arguments.length; i++)
	args.push(arguments[i]);
      args.push(widget);
      
      //call widget and plugin hooks [without loading them onto the object]
      var result = null;
      for(var f in $z.fn)
	if(typeof $z.fn[f]['_' + hook] == 'function')
	  result = $z.fn[f]['_' + hook].apply(self, args);
      if(typeof widget['_' + hook] == 'function')
	result = widget['_' + hook].apply(self, args);
    
      //add type naming function
      if(hook == 'type') return ns;
      
      return result;
    }
    
    //build widget onto an object
    if(obj == null) obj = {};
    
    //add custom plugin and widget methods to the widget instance [building them into the object]
    for(var f in $z.fn)
	Utils.extend(obj, $z.fn[f])
    Utils.extend(obj, widget);
    
    //store options
    obj._ = hook;
    Utils.extend(obj, options);
    
    //hook init
    var result = obj._('init');
    
    //hook load
    obj._('load');
    
    return result;
  };
  
  if(widget == null)
    //if just ns is specified, then assume we're making an arbitrary widget with options as ns
    return makeWidget(null, options, ns, null);
  else
    //namespacing - place makeWidget function for example.widget as $z.example.widget
    Utils.nameSpace(ns, function(options, obj){
      return makeWidget(widget, options, ns, obj);
    });
}
//plugin namespace
$z.fn = {};
//template namespace
$z.tpl = {};
$z.tpl.html = function(tpl){ return tpl; } //the html identity template

//element creation - works with templating system
//builds object if necessary
//nb add template language hook to object
$z.fn.creation = {
  _init: function(widget){
    //if there's already an element (from progressive enhancement for example) don't create
    if(this.$$ != undefined) return this;
    
    //if there's a template, build it
    if(widget._template){
      var lang, html;
      //if a template language is specified, use that
      if(widget._template_language){
	lang = widget._template_language;
	if($z._tpl[lang] == undefined)
	  throw('Zest Error: Template language ' + lang + ' is not defined.');
      }
      //otherwise take most recently defined template language
      else for(lang in $z.tpl) ;
  
      //create template
      var wrapper = document.createElement('DIV');
      wrapper.innerHTML = $z.tpl[lang].call(this, widget._template);
      
      //add the collection references on the managing element
      this.$$ = wrapper.childNodes;
      
      //if an element is defined, add the zest reference
      if(this.$$ != undefined){
	if(this.$$.length > 0)
	  this.$$[0].$z = this;
	else
	  this.$$.$z = this;
      }
      
      //for jQuery, automatically wrap
      //if(jQuery) this.$$ = $(this.$$);
      
      //run attach hooks
      //this._('load');
      
      this._('attach');
      
      //return element
      return this.$$;
    }
    //otherwise run create hooks
    else{
      //run create element, let it decide what to output
      var output = this._('create');
      if(output == null && this.$$ != undefined)
	output = this.$$;

      //if an element is defined, add the zest reference
      if(this.$$ != undefined) this.$$.$z = this;
      
      //if no element is created throw a creation error
      if(this.$$ == null) throw("Zest Error: No creation method for " + this._('type'));
      
      //run attach hooks
      this._('attach');
      
      //return object
      return output;
    }
  }
};

//progressive enhancement
//either should change enhancement to $z.frontpage.feed($('.element'), {});
//or should it be $z($('.element'), 'frontpage.feed', {})

//because $z.frontpage.feed and then having $z('frontpage.feed') on the element being the same
//as the definition is misleading

//but then it might as well be $z($('.element'), {_type: '', ..})
//                          or $z($('.element), [{_type }])
//                          or $z($('.element'), 'frontpage.feed', [{_type }])
//                    and then $z('frontpage.feed', {})
//finally definitions would have to be on the zest widget
//$z.frontpage.feed = {}; from the start

//then could pick up any element with $z($('myelement'))
//and run an action like $z($('.myelement'), 'action')
//or $z($('.myelement')).action();
//or $('.myelement).$z().action(); //hmmm i dont know

//done?
//need to have better function process on $z object!


//actually, stick with it as is, but make it more obvious about registration vs using
//$z.register('frontpage.feed', {_extend: '', asdf});
//$z.create('frontpage.feed', {});
//$z.enhance(parent, recursive = true);
//then enhance runs on an element, but 'enhance selectors' allow for generalised enhancement

//.$z('frontpage.feed')
//.$z() or .$z('action', params) by default checks object or first item in object
//$z.create('frontpage.feed', []);
//[...].$z('frontpage.feed') //would enhance all the elements in an array
//then type overrides only apply on creation

//only other alternative is
//$z.frontpage.feed = {}
//$z('frontpage.feed', {});
//$z.frontpage.feed = $z.extend($z.feed, {
//});

//etc...
//actuall perhaps this is the simplest then????

//then there is also the distinction between data and loading
//personally, i see no reason why data can't be the same as load parameters
//then just ensure load params don't clash with properties or methods
//we also need a load data function, which is just a serialisation
//events run through functions mapped to the create event by the controller
//more complex controller behavior would be good to work out as well
(function(){
  //main enhancement function
  var enhance = function(e, cmd, options){
    //if the element has been processed, then run command (if it exists)
    if(e.$z != undefined){
      if(typeof e.$z[cmd] == 'function'){
        var result = e.$z[cmd](options);
	//if there's no result, allow chaining by default
	return result == null ? e.$z : result;
      }
      return null;
    }
    
    //'command' is the widget to create on the element
    if($z[cmd] == undefined) throw('Zest Error: Can\'t find widget ' + cmd);
    
    //build the widget properties into the DOM element
    //and reference the element from the widget object as well [always a single element] NB $$
    e.$z = $z[cmd](options, {$$: e});
    
    //run the progressive enhancement hook on the element
    e.$z._('enhance');
    
    //possibly update the element references along these lines...
    //if(e.$z.$ != e) e.$z.$.$z = e.$z;
    
    //then run the attach hook for events
    e.$z._('attach');
    
    return e;
  }
  
  //jquery enhancement plugin
  if(jQuery){
    $.fn.$z = function(cmd, options){
      //if the cmd is null, then return the zest object for the nearest element
      if(cmd == null) return this[0].$z;
      //if we pass many elements, return an array
      if(this.length > 1){
        var result = [];
        for(var i = 0; i < this.length; i++)
          result.push(enhance(this[i], cmd, options));
        return result;
      }
      else if(this[0] != null){
	return enhance(this[0], cmd, options);
      }
      else return null;
    }
  }
  //can add other framework plugin methods here
})();


//jQuery selector goodness
$z.fn.selectors = {
  //for creation
  //_init: function(){ if(jQuery) this.$ = function(selector){ return $.merge($(selector, this.$$), $(this.$$).filter(selector)); } },
  //and enhancement
  _load: function(){ if(jQuery) this.$ = function(selector){ return $.merge($(selector, this.$$), $(this.$$).filter(selector)); } }
}

//simple string replacement template language
//
//$z.block({$title: 'hello world', $content: 'it\'s a nice day today'});
//
$z.tpl.simple_template = function(tpl){
  //find all options beginning with a $ sign, and replace with the property on the element
  var html = tpl;
  for(var o in this)
    if(o.substr(0, 1) == '$' && typeof this[o] in {string:1, number:1}){
      html = html.replace(new RegExp('\\\$' + o.substr(1), 'g'), this[o]);
    }
  //if there are any remaining $ vars, just replace them with blank text
  html = html.replace(new RegExp('\\\$[a-zA-Z\-]*', 'g'), '');
   
  return html;
}