/* declare simple simple_carousel javascript plugin */

//simple carousel Zest widget
$z.register('simple_carousel', {
  _enhance: function(){
    var self = this;
    //store basic properties
    this.count = $(this.$$).children().length;
    if(this.scroll == undefined) this.scroll = 1;
    
    //store child elements
    if(this.itemSelector == undefined)
      this.childItems = $(this.$$).children();
    else
      this.childItems = this.$(this.itemSelector);
    
    //let the wrapper act as a clip, and create a clip element around the items
    var wrapper = $('<div class="sclip"><div class="scroll"></div></div>');
    $(wrapper).css('overflow', 'hidden');
    if(this.viewWidth > 0) wrapper.css('width', this.viewWidth);
    if(this.viewHeight > 0) wrapper.css('height', this.viewHeight);

    if(this.vertical){
    }
    else{
      if(this.scrollHeight > 0){
        $('.scroll', wrapper).css('height', this.viewHeight);
      }
      if(this.itemHeight > 0){
        $(this.childItems).css('height', this.itemHeight);
      }
      
      //var width = 0;
      if(this.itemWidth > 0){
        //if a slide width is set, use that
        //width = this.itemWidth * this.childItems.length;
        //set the child widths
        $(this.childItems).css('width', this.itemWidth);
      }
      else{
        //compute total outer width with margin
        //this.childItems.each(function(){
        //  width += $(this).outerWidth(true);
        //});
      }
      $('.scroll', wrapper).css('width', 10000);
      
      //float all the children in the wrapper
      $(this.childItems).css('float', 'left');
    }
    
    
    this.childItems.wrapAll(wrapper);
    $('.scroll', wrapper).css('position', 'relative');
    $('.views-simple-carousel').css('display', 'block');
    
    //create arrows [shouldn't really have click events in an enhance function, but hey]
    if(this.arrows != undefined && this.count > 1){
      $(this.$$).css('position', 'relative');
      //build arrows as div class='left' and div class='right in clip wrapper
      $(this.$$).prepend(
        $("<div class='scroll-prev' />").css({
          position: this.vertical ? 'static' : 'absolute',
          left: 0,
          top: 0,
          width: this.vertical ? '100%' : 'none',
          height: this.vertical ? 'none' : '100%',
          cursor: 'pointer'
	}).click(function(){ self.auto = false; self.prev(); return false; })
      );
      $(this.$$).append(
	$("<div class='scroll-next' />").css({
          position: this.vertical ? 'static' : 'absolute',
          right: 0,
          top: 0,
          width: this.vertical ? '100%' : 'none',
          height: this.vertical ? 'none' : '100%',
	  cursor: 'pointer'
	}).click(function(){ self.auto = false; self.next(); return false; })
      );
      this.$('.scroll-next').append($("<div class='icon' />").click(function(){ self.auto = false; self.next(); return false; }));
      this.$('.scroll-prev').append($("<div class='icon' />").click(function(){ self.auto = false; self.prev(); return false; })).css('display', 'block');
    }
  },
  _attach: function(){
    var self = this;
    //autoscroll functionality
    var elementShown = function(e){
      if(e == document) return true;
      if($(e).css('display') == 'none') return false;
      return elementShown(e.parentNode);
    }
    if(this.auto && this.count > 1){
      var autoScroll = function(){
        setTimeout(function(){
        if(!(self.auto && elementShown(self.$$[0]))) return;
          self.next();
          autoScroll();
        }, self.autointerval ? self.autointerval : 5000);
      }
      autoScroll();
    }
    
    var wrapper = this.$('.sclip');
    
    //click event on each item to show next
    if(this.clickscroll){
      $(this.childItems).each(function(index){
        $(this).click(function(){
          self.scrollTo(index + 1);
          return false;
        });
      });
      $(this.childItems).css('cursor', 'pointer');
    }
  },
  index: 0,
  scrollTo: function(index){
    index = index < this.count ? index : 0;
    index = index < 0 ? 0 : index;
    var self = this;
    this.busy = true;
    
    //take the offset of the index item as the scroll
    var items = this.childItems;
    var dist = 0;
    if(this.vertical)
      dist = $(items[index])[0].offsetTop - this.$('.scroll')[0].offsetTop + $(items[index]).outerHeight() - $(items[index]).innerHeight() - parseInt($(items[index]).css('border-top-width'));
    else
      dist = $(items[index])[0].offsetLeft - this.$('.scroll')[0].offsetLeft + $(items[index]).outerWidth() - $(items[index]).innerWidth() - parseInt($(items[index]).css('border-left-width'));
    
    if(this.vertical)
      anim = {marginTop: -1 * dist};
    else
      anim = {marginLeft: -1 * dist};


    this.$('.scroll').animate(anim, self.scrollspeed ? self.scrollspeed : 300, function(){
      self.busy = false;
      self.index = index;
    });
  },
  next: function(){
    this.scrollTo(this.index + this.scroll);
  },
  prev: function(){
    this.scrollTo(this.index - this.scroll);
  },
  stop: function(){
    this.auto = false;
  }
});
