/* apply simple carousel */

Drupal.behaviors.simple_carousel = function(){
    $('.views-simple-carousel').$z_enhance('simple_carousel', {
        itemSelector: '.views-row',
	arrows: true,
	auto: false,
        vertical: true,
        scroll: 3,
        viewHeight: 400
    });
};