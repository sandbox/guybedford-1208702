<?php

/**
 * @file simple_carousel-view.tpl.php
 * View template to display a view as a simple carousel.
 */
?>
<div class='views-simple-carousel'>
<?php foreach ($rows as $id => $row): ?>
  <div class='views-row'>
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
</div>
